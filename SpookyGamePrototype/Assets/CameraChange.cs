﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraChange : MonoBehaviour {
    public GameObject camera;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

    }
    private void OnTriggerEnter(Collider other)
    {

        camera.gameObject.SetActive(true);
    }

    private void OnTriggerExit(Collider other)
    {

        camera.gameObject.SetActive(false);
    }
}
