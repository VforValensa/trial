﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

    public float moveSpeed = 12f;
    public float turnSpeed = 15.0f;
    private Animator anim;
    

    // Use this for initialization
    void Start () {

        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {

        anim.SetFloat("speed", Input.GetAxis("Vertical"));
        float inputX = Input.GetAxis("Horizontal") * moveSpeed;

        transform.Rotate(new Vector3(0, inputX * turnSpeed * Time.deltaTime, 0));
        //anim.SetBool("isJumping", Input.GetButtonDown("Jump"));
    }
}
