﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchCamera : MonoBehaviour {


    public GameObject camera1;
    public GameObject camera2;
    public bool switched;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnTriggerEnter(Collider other)
    {
        if (!switched)
        {
            camera1.gameObject.SetActive(false);
            camera2.gameObject.SetActive(true);
            switched = true;
        }
        else if (switched)
        {
            camera1.gameObject.SetActive(true);
            camera2.gameObject.SetActive(false);
            switched = false;
        }
    }
}
